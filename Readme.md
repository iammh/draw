#Introduction
Draw in an enginering drawing tool built on the top of fabric JS. The tool has features like drawing basic shapes like Line, Arc, Circle, Polygon and so on. There 
are some addon feature available for the line tool like Snap To Grid, Radial Snap and Sanp-Radial and so on. The project is under development and more features
are going to be added in future like

* Drawing Shape over an Satellite image and detect the area
* Drawing 3D objects 
* Measuring distance from satellite image

#Requirements

* [Fabric JS ](http://fabricjs.com/)
* Material Design CSS
* RequireJS
* Any http server that can serve static content


#Installation Guide

Clone the repository and have it deploy on your webserver. 