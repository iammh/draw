define([
    'jquery',
    'uiUtils',
    'mat'
], function($, uiUtils, mat) {
    'use strict';

    function imageUploadDialog() {
        var options = {
            dismissible: false
        }
        $('body').find("#imageUploadModal").remove();

        var imageUploadDialogDiv = uiUtils.imageUploadDOM();

        $('body').append(imageUploadDialogDiv);

        $("#imageFileInput").on("change", function(e) {
            var file = e.target.files[0];
            var reader = new FileReader();
            reader.onload = function (f) {
              var data = f.target.result;                    
              fabric.Image.fromURL(data, function (img) {
                if(window.backgroundImage) {
                  window.canvas.remove(window.backgroundImage);
                }
                img.selectable = false;
                window.backgroundImage = img;
                window.canvas.add(img).renderAll();
                instances.close();
                instances.destroy();
                $('body').find("#imageUploadModal").remove();
    
              });
            };
            reader.readAsDataURL(file);
          });

        var elem = document.getElementById('imageUploadModal');
       
        var instances = M.Modal.init(elem, options);
        instances.open();

    }


    function radialAngleDialog(actions) {
        var options = {
            dismissible: false
        }
        $('body').find("#radialAngleModal").remove();

        var radialAngleModalDiv = uiUtils.radialAnglePopUpDOM(actions.lineOptions().radialAngle);

        $('body').append(radialAngleModalDiv);

        var elem = document.getElementById('radialAngleModal');

        var select = document.getElementById('angleSelectBox');
        var selectInstance = M.FormSelect.init(select);
        $("#angleSelectBox").on("change", function(e){
            
            actions.lineOptions().radialAngle = Number(e.target.value);
            actions.updateAngle();
            instances.close();
            instances.destroy();
        });
       
        var instances = M.Modal.init(elem, options);
        instances.open();
    }
    
    return {
        imageUploadDialog: function() {
            imageUploadDialog();
        },
        radialAngleDialog: function(actions) {
            radialAngleDialog(actions);
        }
    }
});