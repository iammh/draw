define([
    'utils',
   'line',
   'rectangle',
   'circle',
   'snapToGrid',
   'radialSnap',
   'path',
   'dialog',
   'ellipse',
   'radialGridSnap',
   'arc'
], function(utils,line,rectangle, circle,snapToGrid, radialSnap,path, dialog, ellipse, radialGridSnap,arc) {
    'use strict';

    var activeAction = null;
    var activeToolbarItem = null;
    var lineOptions = {
        isRadialSnap: false,
        isSnapToGrid: false,
        isLine: false,
        radialAngle: 15
    };
    var self;

    

    function init() {
        // console.log("Init ", this);
        utils.generateBackground();
        window.addEventListener('keyup',function(o) {
            
            if(o.keyCode === 76) {
                activeAction = line.getInstance();
                activeAction.enable();
            }
            else if(o.keyCode === 78) {
            //    console.log(activeAction);
                activeAction.isSnapToGrid = false;
            }
            else if(o.keyCode === 77) {
               
                if(activeAction) {
                    activeAction.isSnapToGrid = true;
                }
            }
            else if(o.keyCode === 75) {
                activeAction.disable();
            }
            else if(o.keyCode === 82) {
                activeAction = rectangle.getInstance();
                activeAction.enable();
            }
            else if(o.keyCode === 81) {
                activeAction.disable();
            }
            else if(o.keyCode === 27) { //escape key
                // if(activeAction) {
                    // activeAction.disable();
                    // console.log("Active Escape ", actions.getActiveAction());
                    // utils.itemSelection(activeToolbarItem, self,actions.getActiveAction());
                    utils.escapeLine(actions.getActiveAction());
                    // activeAction = null;
                    // activeAction = null;
                    // activeToolbarItem = null;
                // }
            }
            else if(o.keyCode === 46) {
                var yes = confirm("Are you sure ?");
            if(yes) {
                var activeObject = canvas.getActiveObject();

                if(activeObject) {
                    if(activeObject.id && activeObject.id.indexOf("path-group") > -1) {
                        activeObject._restoreObjectState;
                        canvas.remove(activeObject);
                    }else {
                        if(typeof activeObject.getObjects === "function") {
                            var deleteObjects = activeObject.getObjects();
                            for(var i=0;i< deleteObjects.length;i++) {
                                canvas.remove(deleteObjects[i]);
                            }
                            canvas.discardActiveObject();
                            canvas.requestRenderAll();
                        }else {
                            canvas.remove(activeObject);
                        }
                    }
                }
                

            }
            }
            else {
                // console.log("Key Code ", o.keyCode);
            }
        });


        window.addEventListener('mousedown', function(o) {
            if(o.button === 2) {
                // console.log("Mouse Right ", activeAction);
                
                if(activeAction && ( activeAction.type.indexOf('line') > -1 || activeAction.type.indexOf('radial-snap') > -1 || activeAction.type.indexOf('snap-to-grid') > -1 || activeAction.type.indexOf('radial-grid-snap') > -1)) {
                    // activeAction.disable();
                    utils.escapeLine(actions.getActiveAction());
                }
            }
        });

        // window.oncontextmenu = function(o) {
        //     console.log("Context Menu", o );
        //     return false;
        // }
    }

    function updateAngle() {
        // console.log("Action ",  activeAction);
        if(activeAction && activeAction['setAngle']) {
            activeAction.setAngle(lineOptions.radialAngle);
        }
    }

    function drawLine(elem) {
        

        if(activeAction) {
            if(utils.completeBeforeLeave(activeAction)) {
                activeAction.disable();
            }else {
                return;
            }
        }
        activeToolbarItem = elem;
        lineOptions.isLine = !lineOptions.isLine
        // console.log("Getting a new Line Instance ", elem);
        // console.log("Line Options ",lineOptions);
        // if(!activeAction ||( activeAction && activeAction.type !== 'line')) {
            
        //     activeAction = line.getInstance();
        // }
        if(lineOptions.isSnapToGrid && lineOptions.isRadialSnap) {
            //radial-grid-snap
            activeAction  = radialGridSnap.getInstance();
            activeAction.setAngle(lineOptions.radialAngle);
        }
        else if(lineOptions.isSnapToGrid) {
            activeAction = snapToGrid.getInstance();
        }
        else if(lineOptions.isRadialSnap) {
            activeAction = radialSnap.getInstance();
            activeAction.setAngle(lineOptions.radialAngle);
        }else {
            //if(!activeAction ||( activeAction && activeAction.type !== 'line')) {
                
                activeAction = line.getInstance();
            //}
        }
        utils.itemSelection(activeToolbarItem, self,activeAction);
        
        // activeAction.enable();
    }

    function drawRectangle (elem) {
        
        if(activeAction) {
            // console.log("Inside Something ", elem);
            if(utils.completeBeforeLeave(activeAction)) {
                activeAction.disable();
            }else {
                return;
            }
        }
        activeToolbarItem = elem;
        // console.log("Getting a new Rectangle Instance ", elem);
        if(!activeAction ||( activeAction && activeAction.type !== 'rectangle')) {
            
            activeAction = rectangle.getInstance();
        }
        utils.itemSelection(activeToolbarItem, self,activeAction);
    }

    function snapToGridLine(elem) {
        if(activeAction) {
            if(utils.completeBeforeLeave(activeAction)) {
                activeAction.disable();
                activeAction = null;
            }else {
                return;
            }
            // activeAction.disable();
            // activeAction = null;
        }
        lineOptions.isSnapToGrid = !lineOptions.isSnapToGrid;
        activeToolbarItem = elem;
        // console.log("Getting a new Line Instance ", elem);

        if(lineOptions.isRadialSnap) {
            activeAction = radialGridSnap.getInstance();
            utils.itemSelection(activeToolbarItem, self,activeAction);
            return;
        }
        
        if(!activeAction ||( activeAction && activeAction.type !== 'snap-to-grid')) {
            
            activeAction = snapToGrid.getInstance();
        }
       

        utils.itemSelection(activeToolbarItem, self,activeAction);
    }

    function drawCircle(elem) {
        if(activeAction) {
            if(utils.completeBeforeLeave(activeAction)) {
                activeAction.disable();
            }else {
                return;
            }
        }
        activeToolbarItem = elem;
        // console.log("Getting a new Line Instance ", elem);
        if(!activeAction ||( activeAction && activeAction.type !== 'circle')) {
            
            activeAction = circle.getInstance();
        }
        utils.itemSelection(activeToolbarItem, self,activeAction);
    }

    function drawRadialSnapLine(elem, sActions) {
        if(activeAction) {
            if(utils.completeBeforeLeave(activeAction)) {
                activeAction.disable();
                activeAction = null;
            }else {
                return;
            }
            // activeAction.disable();
            // activeAction = null;
        }
        dialog.radialAngleDialog(sActions);
        lineOptions.isRadialSnap = true;
        
        activeToolbarItem = elem;
        // console.log("Getting a new Line Instance ", elem);
        if(lineOptions.isSnapToGrid) {
            activeAction = radialGridSnap.getInstance();
            utils.itemSelection(activeToolbarItem, self,activeAction);
            return;
        }
        if(!activeAction ||( activeAction && activeAction.type !== 'radial-snap')) {
            
            activeAction = radialSnap.getInstance();
        }
        
        utils.itemSelection(activeToolbarItem, self,activeAction);
    }

    function disableAction() {
        activeAction.disable();
        activeAction = null;
    }

    function clear() {
        if(activeAction) {
            activeAction.disable();
        }
        var ok = confirm("This will sweap everything. Would you like to continue?");
        if(ok) {
            canvas.clear();
            utils.generateBackground();
        }
        // if(canvas.getActiveObject()) {
        //     var ok = confirm("Are you sure?");
        //     if(ok)
        //         canvas.remove(canvas.getActiveObject());
        // }else {
        //     var ok = confirm("This will sweap everything. Would you like to continue?");
        //     if(ok) {
        //         canvas.clear();
        //         utils.generateBackground();
        //     }
        // }
    }

    function drawPath(elem) {
        if(activeAction) {
            if(utils.completeBeforeLeave(activeAction)) {
                activeAction.disable();
            }else {
                return;
            }
        }
        activeToolbarItem = elem;
        
        if(!activeAction ||( activeAction && activeAction.type !== 'path')) {
            
            activeAction = path.getInstance();
        }
        utils.itemSelection(activeToolbarItem, self,activeAction);
    }

    function imageUploadDialog() {
        if(activeAction) {
            activeAction.disable();
        }
        dialog.imageUploadDialog();
    }

    function drawEllipse(elem) {
        if(activeAction) {
            if(utils.completeBeforeLeave(activeAction)) {
                activeAction.disable();
            }else {
                return;
            }
        }
        activeToolbarItem = elem;
        if(!activeAction ||( activeAction && activeAction.type !== 'ellipse')) {
            
            activeAction = ellipse.getInstance();
        }
        utils.itemSelection(activeToolbarItem, self,activeAction);
    }

    function drawArcTool(elem) {
        activeAction = arc.getInstance();
        activeToolbarItem = elem;
        utils.itemSelection(activeToolbarItem, self, activeAction);
    }

    // line.getInstance().enable();
    return {
        init: function() {
            self = this;
            init();
        },
        lineOptions: function() {
            return lineOptions;
        },
        resetLineOptions: function() {
            // console.log("Reset Line option called");
            lineOptions = {
                isRadialSnap: false,
                isSnapToGrid: false,
                isLine: false,
                radialAngle: 15
            };
        },
        drawLine: function(elem) {
            drawLine(elem);
        },
        drawRectangle: function (elem) {
            drawRectangle(elem);
        },
        snapToGridLine: function(elem) {
            snapToGridLine(elem);
        },
        drawCircle: function(elem) {
            drawCircle(elem);
        },
        drawRadialSnapLine: function(elem) {
            
            drawRadialSnapLine(elem, this);
        },
        disableAction: function () {
            disableAction();
        },
        clear: function () {
            clear();
        },
        drawPath: function(elem) {
            drawPath(elem);
        },
        imageUploadDialog: function() {
            imageUploadDialog();
        },
        drawEllipse: function(elem) {
            drawEllipse(elem);
        },
        updateAngle: function() {
            updateAngle();
        },
        getActiveAction: function() {
            return activeAction;
        },
        setActiveAction: function(l) {
            activeAction = l;
        },
        clearRadialSnap: function() {
            utils.clearRadialSnap();
        },
        drawArcTool: function(elem) {
            drawArcTool(elem);
        }
    }
});