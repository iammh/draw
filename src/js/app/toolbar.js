define([
    'jquery',
    'actions'
], function($, actions) {
    'use strict';
    return function() {

        $('#clearCanvas').click(function() {
            // canvas.clear();
            actions.clear();
        });

        $('#lineTool').click(function() {
            actions.drawLine(this);
        });

        $('#rectangleTool').click(function() {
            actions.drawRectangle(this);
        });

        $('#snapToGrid').click(function() {
            actions.snapToGridLine(this);
            
        });

        $('#circleTool').click(function() {
        
            actions.drawCircle(this);
        });

        $('#radialSnapTool').click(function() {
            actions.drawRadialSnapLine(this);
            // $(this).addClass("activeItem");
        });

        $('#pathTool').click(function() {
            
            actions.drawPath(this);
            // $(this).addClass("activeItem");
        });

        $("#imageUploadTool").click(function () {
            actions.imageUploadDialog();
        });

        $("#ellipseTool").click(function () {
            actions.drawEllipse(this);
        });

        $("#arcTool").click(function () {
            actions.drawArcTool(this);
        });



        
    }
});