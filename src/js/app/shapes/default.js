define([
], function() {
    'use strict';
    function Arc() {
        this.isMouseDown = false;
        function mousedown(o) {
            
           
        }
        function mousemove(o) {
           
            
        }
        function mouseup(o) {
            var pointer = canvas.getPointer(o.e);
            

        }

        this.enable = function () {
            
            canvas.on('mouse:down', mousedown);
            canvas.on('mouse:move', mousemove);
            canvas.on('mouse:up', mouseup);
            self.isEnabled = true;
            
        }

        this.completePath = function () {
           
        }

        this.disable = function () {
            
            self.isMouseDown = false;

            self.isEnabled = false;
            canvas.selection = true;
            canvas.off('mouse:down', mousedown);
            canvas.off('mouse:move', mousemove);
            canvas.off('mouse:up', mouseup);

            self.temp = [];
            self.points = [];
            console.log("Disabling line Tool");
            actions.setActiveAction(null);
        }
    }
    return {
        getInstance: function() {
            return new Arc();
        }
    }
});