define(['utils', 'colors'], function(utils, colors) {
    'use strict';
    function Ellipse() {
        this.type='ellipse';
        this.strokeColor = colors.strokeColor;
        this.isEnabled = false;

        this.origin = 0;
        this.isMouseDown = false;
        this.drawnObject = null;
        var self = this;
        function mousedown(o) {
            var pointer = canvas.getPointer(o.e);
            self.origin = {
                x: pointer.x,
                y: pointer.y
            };

            canvas.selection = false;
            self.isMouseDown = true;

            self.drawnObject = new fabric.Ellipse({
                left: pointer.x,
                top: pointer.y,
                originX: 'left',
                originY: 'top',
                rx: pointer.x-pointer.x,
                ry: pointer.y-pointer.y,
                angle: 0,
                fill: '',
                stroke: colors.strokeColor,
                strokeWidth:1,
            });
            canvas.add(self.drawnObject);
           
        }
        function mousemove(o) {
            if(!self.isMouseDown) return;
            var pointer = canvas.getPointer(o.e);

            var rx = Math.abs(self.origin.x - pointer.x)/2;
            var ry = Math.abs(self.origin.y - pointer.y)/2;
            if (rx > self.drawnObject.strokeWidth) {
            rx -= self.drawnObject.strokeWidth/2
            }
            if (ry > self.drawnObject.strokeWidth) {
            ry -= self.drawnObject.strokeWidth/2
            }
            self.drawnObject.set({ rx: rx, ry: ry});
            
            if(self.origin.x>pointer.x){
                self.drawnObject.set({originX: 'right' });
            } else {
                self.drawnObject.set({originX: 'left' });
            }
            if(self.origin.y>pointer.y){
                self.drawnObject.set({originY: 'bottom'  });
            } else {
                self.drawnObject.set({originY: 'top'  });
            }
            canvas.renderAll();

        }
        function mouseup(o) {
            self.isMouseDown = false;
            self.drawnObject.setCoords();

            canvas.discardActiveObject();
            canvas.setActiveObject(self.drawnObject);
            canvas.renderAll();
            utils.disableAction(self);

            $("#ellipseTool").removeClass("activeItem");
        }

        this.enable = function () {
            
            canvas.on('mouse:down', mousedown);
            canvas.on('mouse:move', mousemove);
            canvas.on('mouse:up', mouseup);
            
        }

        this.disable = function () {
            
            self.isMouseDown = false;
            canvas.selection = true;
            canvas.off('mouse:down', mousedown);
            canvas.off('mouse:move', mousemove);
            canvas.off('mouse:up', mouseup);
            actions.setActiveAction(null);
            

            $("#ellipseTool").removeClass("activeItem");
            actions.setActiveAction(null);      
        }
    }

    return {
        getInstance: function() {
            return new Ellipse();
        }
    }
});