define(['utils', 'colors'], function(utils, colors) {
    'use strict';

    function Line() {
        this.type='line';
        this.isEnabled = false;
        this.strokeColor = colors.strokeColor;
        this.isMouseDown = false;
        this.drawnObject = null;
        this.points = [];
        this.temp = [];
        var self = this;
        function mousedown(o) {
            var pointer = canvas.getPointer(o.e);
            canvas.selection = false;
            self.isMouseDown = true;
            var l = new fabric.Line([pointer.x, pointer.y,pointer.x, pointer.y], {
                stroke: self.strokeColor,
                fill: ''
            });
            self.drawnObject = l;

            l.perPixelTargetFind = true;
            self.temp.push(l);
            self.drawnObject.perPixelTargetFind = true;
            
            
            canvas.add(l);
        }
        function mousemove(o) {
            // console.log("Snap to grid",self.isSnapToGrid);
            if(self.isMouseDown) {
                var pointer = canvas.getPointer(o.e);
                self.drawnObject.set({x2: pointer.x, y2: pointer.y});
                self.points.push({x: pointer.x, y: pointer.y});
                
                canvas.renderAll();
            }
        }
        function mouseup(o) {
            var pointer = canvas.getPointer(o.e);
            
            //self.isMouseDown = false;
            //canvas.selection = true;
            
            // self.drawnObject.setCoords();
            // canvas.renderAll();

            // canvas.discardActiveObject();
            // canvas.setActiveObject(self.drawnObject);

            
            // utils.disableAction(self);
        }

        this.enable = function () {
            // console.log("Setting up");
            canvas.on('mouse:down', mousedown);
            canvas.on('mouse:move', mousemove);
            canvas.on('mouse:up', mouseup);
            self.isEnabled = true;
            
        }

        this.completePath = function () {
           if(self.temp.length > 1) {
               
                var group = new fabric.Group(self.temp, {
                    // left: self.temp[0].left,
                    // top: self.temp[0].top,
                    id: 'path-group'+ (Math.random() * 100)
                });
                
                group.setCoords();

                self.temp.forEach(function(item) {
                    canvas.remove(item);
                });
    
                canvas.add(group);
                self.drawnObject = group;
                canvas.renderAll();
                
           }else {
               if(self.drawnObject) {
                self.drawnObject.setCoords();
                canvas.renderAll(); 
               }
            
           }
        }

        this.disable = function () {
            // console.log("Disable Mouse Control");
            self.isMouseDown = false;
            // if(self.drawnObject) {
            //     self.drawnObject.setCoords();
            // }
            this.completePath();
            self.isEnabled = false;
            canvas.selection = true;
            canvas.off('mouse:down', mousedown);
            canvas.off('mouse:move', mousemove);
            canvas.off('mouse:up', mouseup);

            self.temp = [];
            self.points = [];
            
            actions.setActiveAction(null);
        }
    }

    

    return {
        getInstance: function() {
            return new Line()
        }
    };
});