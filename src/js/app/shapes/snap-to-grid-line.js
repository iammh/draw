define(['utils', 'colors'], function(utils, colors) {
    'use strict';

    function SnapToGridLine() {
        this.type = 'snap-to-grid';
        this.strokeColor = colors.strokeColor;
        this.isEnabled = false;

        this.isSnapToGrid = !this.isSnapToGrid;
        this.isMouseDown = false;
        this.drawnObject = null;
        this.temp = [];
        var self = this;
        function mousedown(o) {
            var pointer = canvas.getPointer(o.e);
            canvas.selection = false;
            self.isMouseDown = true;
            if(!self.drawnObject) { 
                self.drawnObject = new fabric.Line([pointer.x, pointer.y,pointer.x, pointer.y], {
                    stroke: self.strokeColor,
                    fill: ''
                });
            }else {
                self.drawnObject = new fabric.Line([self.drawnObject.x2, self.drawnObject.y2,pointer.x, pointer.y], {
                    stroke: self.strokeColor,
                    fill: ''
                });
            }
            self.temp.push(self.drawnObject);
            // self.drawnObject.perPixelTargetFind = true;
            canvas.add(self.drawnObject);
        }
        function mousemove(o) {
            // console.log("Snap to grid",self.isSnapToGrid);
            if(self.isMouseDown) {
                var pointer = canvas.getPointer(o.e);

                if(self.isSnapToGrid) {
                    var corrected = {
                        x2: Math.round(pointer.x / 20) * 20,
                        y2: Math.round(pointer.y / 20) * 20
                    };
                    
                    var x1 = self.drawnObject.x1;
                    var y1 = self.drawnObject.y1;
                    var x2 = pointer.x;
                    var y2 = pointer.y;
    
                    var x = corrected.x2;
                    var y = corrected.y2;
                    var decesion = Math.abs(x1-x2);
                    if( decesion >= 0 && decesion <= 50) {
                        x = (((x1-x2) / (y1-y2)) * (y - y1)) + x1;
                    } else {
                        y = (((y1 - y2) / (x1-x2)) * (x- x1)) + y1;
                    }
    
                    
                    self.drawnObject.set({x2:x,y2:y});
                    if(self.temp.length>0) {
                        self.temp[self.temp.length-1].set({x2:x,y2:y});
                    }
                }
                
                // self.drawnObject.set({x2: pointer.x, y2: pointer.y});
                canvas.renderAll();
            }
        }
        function mouseup(o) {
            var pointer = canvas.getPointer(o.e);
            // if(self.isSnapToGrid) {
            //     var corrected = {
            //         x2: Math.round(self.drawnObject.x2 / 20) * 20,
            //         y2: Math.round(self.drawnObject.y2 / 20) * 20
            //     };
                
            //     var x1 = self.drawnObject.x1;
            //     var y1 = self.drawnObject.y1;
            //     var x2 = self.drawnObject.x2;
            //     var y2 = self.drawnObject.y2;

            //     var x = corrected.x2;
            //     var y = corrected.y2;
            //     var decesion = Math.abs(x1-x2);
            //     if( decesion >= 0 && decesion <= 50) {
            //         x = (((x1-x2) / (y1-y2)) * (y - y1)) + x1;
            //     } else {
            //         y = (((y1 - y2) / (x1-x2)) * (x- x1)) + y1;
            //     }

                
            //     self.drawnObject.set({x2:x,y2:y});
            // }
            // self.isMouseDown = false;
            // canvas.selection = true;
            
            // self.drawnObject.setCoords();
            // canvas.renderAll();

            // canvas.discardActiveObject();
            // canvas.setActiveObject(self.drawnObject);

            
            // utils.disableAction(self);
        }

        this.enable = function () {
            // console.log("Setting up");
            canvas.on('mouse:down', mousedown);
            canvas.on('mouse:move', mousemove);
            canvas.on('mouse:up', mouseup);
            self.isEnabled = true;
            
            
        }

        this.completePath = function() {
            if(self.temp.length > 1) {
               
                var group = new fabric.Group(self.temp, {
                    id: 'path-group'+ (Math.random() * 100)
                });
                
                group.setCoords();

                self.temp.forEach(function(item) {
                    canvas.remove(item);
                });
                self.temp = [];
                canvas.add(group);
                self.drawnObject = group;
                canvas.renderAll();
                
           }
        }

        this.disable = function () {
            // console.log("Disable Mouse Move");
            canvas.off('mouse:down', mousedown);
            canvas.off('mouse:move', mousemove);
            canvas.off('mouse:up', mouseup);

            self.isMouseDown = false;
            self.isEnabled = false;
            canvas.selection = true;
            if(self.drawnObject) {
                self.drawnObject.setCoords();
                canvas.discardActiveObject();
                canvas.setActiveObject(self.drawnObject);
                self.completePath();
                canvas.renderAll();
                
                
            }
            actions.setActiveAction(null);
            $('#snapToGrid').removeClass('activeItem');
        }
    }

    

    return {
        getInstance: function() {
            return new SnapToGridLine()
        }
    };
});