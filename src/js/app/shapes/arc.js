define([
    'utils',
    'colors'
], function(utils, colors) {
    'use strict';

    function Rectangle() {
        this.type = 'arc';
        this.strokeColor = colors.strokeColor;
        this.isEnabled = false;
        
        this.isMouseDown = false;
        this.drawnObject = null;
        var self = this;
        canvas.defaultCursor = "crosshair";
        function mousedown(o) {
            var pointer = canvas.getPointer(o.e);
            canvas.selection = false;
            self.isMouseDown = true;
           
        }
        function mousemove(o) {
            if(self.isMouseDown) {
                var pointer = canvas.getPointer(o.e);
                var newWidth = (self.drawnObject.left - pointer.x) * -1;
                var newHeight = (self.drawnObject.top - pointer.y) * -1;
                self.drawnObject.set({width: newWidth, height: newHeight});
                canvas.renderAll();
            }
        }
        function mouseup(o) {
            
            
            // if (self.drawnObject.width < 0) {
            //     var newLeft = self.drawnObject.left + self.drawnObject.width;
            //     var newWidth = Math.abs(self.drawnObject.width);
            //     self.drawnObject.set({left: newLeft, width: newWidth});
            // }
        
            // if (self.drawnObject.height < 0) {
            //     var newTop = self.drawnObject.top + self.drawnObject.height;
            //     var newHeight = Math.abs(self.drawnObject.height);
            //     self.drawnObject.set({top: newTop, height: newHeight});
            // }

            // self.drawnObject.setCoords();
            // canvas.defaultCursor = "default";
            // if(self.drawnObject)
            //     canvas.setActiveObject(self.drawnObject);
            // canvas.renderAll();
            utils.disableAction(self); 
            
        }

        this.drawArc = function() {
            var pointer = utils.getPoint();
            var c = new fabric.Circle({
                id: 'arcCircle',
                originX: 'center',
                originY: 'center',
                left: pointer.x,
                top: pointer.y,
                stroke: self.strokeColor,
                radius: 150,
                startAngle: Math.PI,
                fill: ''
            });

            self.drawnObject = c;

            canvas.add(c);
        }

        this.enable = function () {
            
            // canvas.on('mouse:down', mousedown);
            // canvas.on('mouse:move', mousemove);
            // canvas.on('mouse:up', mouseup);
            self.isEnabled = true;

            self.drawArc();
            self.disable();
            
        }

        this.disable = function () {
            
            self.isMouseDown = false;
            self.isEnabled = false;
            canvas.selection = true;
            // canvas.off('mouse:down', mousedown);
            // canvas.off('mouse:move', mousemove);
            // canvas.off('mouse:up', mouseup);

            canvas.selection = true;
            canvas.discardActiveObject();
            if(self.drawnObject)
                canvas.setActiveObject(self.drawnObject);
            
            // console.log("Active Action Rectangle ", actions.activeAction());
            actions.setActiveAction(null);
            setTimeout(function() {
                $("#arcTool").removeClass("activeItem");
            }, 200)
        }
    }

    

    return {
        getInstance: function() {
            return new Rectangle()
        }
    };
    
});