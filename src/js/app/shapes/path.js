define(['utils', 'colors'], function(utils, colors) {
    'use strict';

    function Path() {
        this.type='path';
        this.isEnabled = false;
        this.strokeColor = colors.strokeColor;
        this.pathIndex = 0;
        this.mousedownCount = 0;

        this.isMouseDown = false;
        this.drawnObject = null;
        this.points = [];
        this.circles = [];
        this.temp = [];
        var self = this;
        function mousedown(o) {
            console.log("Single Click ", o.e.detail === 1);
            self.isMouseDown = true;
            setTimeout(function() {
                if(o.e.detail === 1) {
                    var pointer = canvas.getPointer(o.e);
                    self.points.push({x: pointer.x, y: pointer.y});
                    self.mousedownCount++;
                    if(self.mousedownCount >= 1 && self.mousedownCount < 2) {
                        console.log("Drawing first circle");
                        var c = new fabric.Circle({
                            radius: 3,
                            left: pointer.x,
                            top: pointer.y,
                            fill: '',
                            stroke: self.strokeColor,
                            selectable: false
                        });
                        self.drawnObject = c;
                        canvas.add(self.drawnObject);
                    }else {
                        if(self.drawnObject) {
                            canvas.remove(self.drawnObject);
                        }
                        self.drawLine();
                    }
                }
            }, 300);
            

        }
        function mousemove(o) {
            
        }
        function mouseup(o) {
            var pointer = canvas.getPointer(o.e);
            
           
        }

        function doubleClick(o) {
            
            console.log("Double click ", o);
            self.disable();
            
        }

        this.enable = function () {
            // console.log("Setting up");
            canvas.on('mouse:down', mousedown);
            canvas.on('mouse:move', mousemove);
            canvas.on('mouse:up', mouseup);
            canvas.on('mouse:dblclick', doubleClick);
            self.isEnabled = true;
            
        }

        this.completePath = function () {
            
            var obj = utils.getAllObjects();
            console.log("All objects ", obj);
            for(var i=0;i<obj.length;i++) {
                if(obj[i] === 'emptyCircle') {
                    canvas.remove(obj[i]);
                    canvas.renderAll();
                }
            }
            // self.points.pop();
        
            var endPoint = self.points[self.points.length-1];
            var startPoint = self.points[0];

            var line = new fabric.Line([endPoint.x,endPoint.y,startPoint.x,startPoint.y], {
                name: 'pathLine',
                stroke: self.strokeColor,
                selectable: false
            });

            var start = new fabric.Circle({
                radius: 3,
                stroke: self.strokeColor,
                left: endPoint.x,
                top: endPoint.y,
                selectable: false,
                fill: '',
                name: 'lnCircle'
            });


            var end = new fabric.Circle({
                radius: 3,
                stroke: self.strokeColor,
                left: startPoint.x,
                top: startPoint.y,
                fill: '',
                selectable: false,
                name: 'lnCircle'
            });

            var lineGroup = new fabric.Group([start, line, end], {
                id: 'path-group-'+Math.random()*100
            });
            lineGroup.setCoords();

            canvas.add(lineGroup);
            // self.mousedownCount = 0;
            canvas.defaultCursor = 'default';
        }

        this.disable = function () {
            // console.log("Disable Mouse Control");
            self.isMouseDown = false;
            if(self.drawnObject) {
                self.drawnObject.setCoords();
            }
            this.completePath();
            self.isEnabled = false;
            canvas.selection = true;
            canvas.off('mouse:down', mousedown);
            canvas.off('mouse:move', mousemove);
            canvas.off('mouse:up', mouseup);
            canvas.off('mouse:dblclick', doubleClick);
            

            self.temp = [];
            self.pathIndex = 0;
            self.circles = [];
            self.points = [];
            $("#pathTool").removeClass("activeItem");
            actions.setActiveAction(null);
        }

        this.drawLine = function() {
            var last = self.points.length-1;
            var A = self.points[last-1];
            var B = self.points[last];
            var line = new fabric.Line([A.x,A.y,B.x,B.y], {
                name: 'pathLine',
                selectable: false,
                stroke: self.strokeColor
            });

            var start = new fabric.Circle({
                radius: 3,
                fill: '',
                selectable: false,
                stroke: self.strokeColor,
                left: A.x,
                top: A.y,
                name: 'lnCircle'
            });


            var end = new fabric.Circle({
                radius: 3,
                fill: '',
                selectable: false,
                stroke: self.strokeColor,
                left: B.x,
                top: B.y,
                name: 'lnCircle'
            });

            var lineGroup = new fabric.Group([start, line, end], {
                id: 'path-group-'+Math.random()*100
            });
            
            lineGroup.setCoords();

            canvas.add(lineGroup);

        }   
    }


    return {
        getInstance: function() {
            return new Path()
        }
    };
});