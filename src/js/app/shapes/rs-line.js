define(['utils', 'colors'], function(utils, colors) {
    'use strict';

    function RadialGridSnapLine() {
        this.type = 'radial-grid-snap';
        this.strokeColor = colors.strokeColor;
        this.strokeColor = colors.strokeColor;
        this.isEnabled = false;

        this.isMouseDown = false;
        this.drawnObject = null;
        this.rotation_origin = null;
        this.gridAngle = 45;
        this.temp = [];
        this.angle_radians = fabric.util.degreesToRadians(this.gridAngle);
        

        var self = this;
        function mousedown(o) {
            var pointer = canvas.getPointer(o.e);
            canvas.selection = false;
            self.isMouseDown = true;
            self.rotation_origin = new fabric.Point(pointer.x, pointer.y);

            self.drawnObject = new fabric.Line([pointer.x, pointer.y,pointer.x, pointer.y], {
                stroke: self.strokeColor,
                strokeWidth: 1,
                name: '1line',
                selectable: true
            });  
            
            canvas.add(self.drawnObject);
        }
        function mousemove(o) {
            // console.log("Radial Snap is Mouse down",self.isMouseDown);
            if(self.isMouseDown) {
                var pointer = canvas.getPointer(o.e);
                var startX = self.rotation_origin.x;
                var startY = self.rotation_origin.y;
                var x2 = pointer.x - startX;
                var y2 = pointer.y - startY;

                var cosx = 0;
                var sinx = 0;
                
                if(self.gridAngle === 90) {
                    

                    var r = Math.sqrt(x2*x2 + y2*y2);
                    var angle = (Math.atan2(y2, x2) / Math.PI * 180);
                    
                    angle = (angle) % 360 + 180;
                    
                    if (angle <= 45 || angle >= 315) {
                        angle = 0;
                    } else if (angle <= 135) {
                        angle = 90;
                    } else if (angle <= 225) {
                        angle = 180;  
                    } else if (angle <= 315) {
                        angle = 270; 
                    }
                    angle -= 180;
                    
                    var cosx = r * Math.cos(angle * Math.PI / 180);
                    var sinx = r * Math.sin(angle * Math.PI / 180);
                    
                    self.drawnObject.set({
                        x2: cosx + startX,
                        y2: sinx + startY
                    });
                    
                    canvas.renderAll();

                    
                }else if(self.gridAngle === 45) {
                    

                    var r = Math.sqrt(x2*x2 + y2*y2);
                    var angle = (Math.atan2(y2, x2) / Math.PI * 180);
                    
                    angle = (angle) % 360 + 180;
                    
                    if (angle <= 22.5 || angle >= 337.5) {
                        angle = 0;
                    } else if (angle <= 67.5) {
                        angle = 45;
                    } else if (angle <= 112.5) {
                        angle = 90;  
                    } else if (angle <= 157.5) {
                        angle = 135; 
                    } else if (angle <= 202.5) {
                        angle = 180;
                    } else if (angle <= 247.5) {
                        angle = 225;
                    } else if (angle <= 292.5) {
                        angle = 270;
                    } else if (angle < 337.5) {
                        angle = 315;
                    }
                    angle -= 180;
                    
                    cosx = r * Math.cos(angle * Math.PI / 180);
                    sinx = r * Math.sin(angle * Math.PI / 180);
                    
                    
                } else if(self.gridAngle === 30) {
                    

                    var r = Math.sqrt(x2*x2 + y2*y2);
                    var angle = (Math.atan2(y2, x2) / Math.PI * 180);
                    
                    angle = (angle) % 360 + 180;
                    
                    if (angle <= 15 || angle >= 345) {
                        angle = 0;
                    } else if (angle <= 45) {
                        angle = 30;
                    } else if (angle <= 75) {
                        angle = 60;  
                    } else if (angle <= 105) {
                        angle = 90; 
                    } else if (angle <= 135) {
                        angle = 120;
                    } else if (angle <= 165) {
                        angle = 150;
                    } else if (angle <= 195) {
                        angle = 180;
                    } else if (angle <= 225) {
                        angle = 210;
                    } else if (angle < 255) {
                        angle = 240;
                    } else if (angle < 285) {
                        angle = 270;
                    } else if (angle < 315) {
                        angle = 300;
                    } else if (angle < 345) {
                        angle = 330;
                    }

                    angle -= 180;
                    
                    cosx = r * Math.cos(angle * Math.PI / 180);
                    sinx = r * Math.sin(angle * Math.PI / 180);
                    
                    
                } else if(self.gridAngle === 15) {
                    

                    var r = Math.sqrt(x2*x2 + y2*y2);
                    var angle = (Math.atan2(y2, x2) / Math.PI * 180);
                    
                    angle = (angle) % 360 + 180;
                    
                    if (angle <= 7.5 || angle >= 352.5) {
                        angle = 0;
                    } else if (angle <= 22.5) {
                        angle = 15;
                    } else if (angle <= 37.5) {
                        angle = 30;  
                    } else if (angle <= 52.5) {
                        angle = 45; 
                    } else if (angle <= 67.5) {
                        angle = 60;
                    } else if (angle <= 82.5) {
                        angle = 75;
                    } else if (angle <= 97.5) {
                        angle = 90;
                    } else if (angle <= 112.5) {
                        angle = 105;
                    } else if (angle < 127.5) {
                        angle = 120;
                    } else if (angle < 142.5) {
                        angle = 135;
                    } else if (angle < 157.5) {
                        angle = 150;
                    } else if (angle < 172.5) {
                        angle = 165;
                    } else if (angle <= 187.5) {
                        angle = 180;
                    } else if (angle <= 202.5) {
                        angle = 195;  
                    } else if (angle <= 217.5) {
                        angle = 210; 
                    } else if (angle <= 232.5) {
                        angle = 225;
                    } else if (angle <= 247.5) {
                        angle = 240;
                    } else if (angle <= 262.5) {
                        angle = 255;
                    } else if (angle <= 277.5) {
                        angle = 270;
                    } else if (angle < 292.5) {
                        angle = 285;
                    } else if (angle < 307.5) {
                        angle = 300;
                    } else if (angle < 322.5) {
                        angle = 315
                    } else if (angle < 337.5) {
                        angle = 330
                    } else if (angle < 352.5) {
                        angle = 345;
                    }

                    angle -= 180;
                    
                    cosx = r * Math.cos(angle * Math.PI / 180);
                    sinx = r * Math.sin(angle * Math.PI / 180);
                    
                    
                }


                var corrected = {
                    x2: Math.round(pointer.x / 20) * 20,
                    y2: Math.round(pointer.y / 20) * 20
                };

                var anglePoint = {
                    x: cosx + startX,
                    y: sinx + startY
                };

                var x1 = self.drawnObject.x1;
                var y1 = self.drawnObject.y1;
                var x2 = anglePoint.x;
                var y2 = anglePoint.y;

                var x = corrected.x2;
                var y = corrected.y2;
                var decesion = Math.abs(x1-x2);
                if( decesion >= 0 && decesion <= 50) {
                    x = (((x1-x2) / (y1-y2)) * (y - y1)) + x1;
                } else {
                    y = (((y1 - y2) / (x1-x2)) * (x- x1)) + y1;
                }

                self.drawnObject.set({x2: x, y2: y});
                canvas.renderAll();
            }
        }
        function mouseup(o) {
            
            self.isMouseDown = false;
            canvas.selection = true;
            
            self.drawnObject.setCoords();
            canvas.renderAll();

            canvas.discardActiveObject();
            canvas.setActiveObject(self.drawnObject);

            
            utils.disableAction(self);
        }

        this.enable = function () {
            // console.log("Setting up");
            canvas.on('mouse:down', mousedown);
            canvas.on('mouse:move', mousemove);
            canvas.on('mouse:up', mouseup);
            self.isEnabled = true;
            
        }

        this.completeLine = function () {
            // if(self.temp.length > 1) {
            //     //console.log("Lines ", self.temp);
            //      var group = new fabric.Group(self.temp, {
            //          id: 'path-group'+ (Math.random() * 100)
            //      });
                 
            //      group.setCoords();
 
            //      self.temp.forEach(function(item) {
            //         canvas.remove(item);
            //      });
            //      self.temp =[]
            //      canvas.remove(self.drawnObject);

            //      self.drawnObject = group;
            //      canvas.add(group);
            //      canvas.renderAll();
                 
                 
            // }
         }

        this.disable = function () {
            console.log("Disabling");
            canvas.off('mouse:down', mousedown);
            canvas.off('mouse:move', mousemove);
            canvas.off('mouse:up', mouseup);
            canvas.selection = true;
            self.isEnabled = false;
            self.isMouseDown = false;
            canvas.selection = true;
            
            if(self.drawnObject) {
                console.log("Disabling self.Drawing obj");
                self.drawnObject.setCoords();
                self.completeLine();
                canvas.discardActiveObject();
                canvas.setActiveObject(self.drawnObject);
                canvas.renderAll();
                actions.lineOptions().isLine = false;
                actions.lineOptions().isRadialSnap = false;
                actions.lineOptions().isSnapToGrid = false;
                actions.lineOptions().radialAngle = 15;
            }
           

           
            actions.setActiveAction(null);
            
            // utils.disableAction(self);

            $('.toolbar-item').removeClass("activeItem");
            $('#lineTool').removeClass("activeItem");
            // actions.lineOptions().isRadialSnap = false;
            // actions.lineOptions().isSnapToGrid = false;
            // actions.lineOptions().radialAngle = 15;
            
        }

        this.setAngle = function(angle) {
            // console.log("Setting angle ", angle);
            this.gridAngle = angle;
            this.angle_radians = fabric.util.degreesToRadians(angle);
        }
    }

    

    return {
        getInstance: function() {
            return new RadialGridSnapLine();
        }
    };
});