define(['utils', 'colors'], function(utils, colors) {
    'use strict';
    function Circle() {
        this.type='circle';
        this.strokeColor = colors.strokeColor; 
        this.isEnabled = false;

        this.origin = 0;
        this.isMouseDown = false;
        this.drawnObject = null;
        var self = this;
        function mousedown(o) {
            var pointer = canvas.getPointer(o.e);
            self.origin = {
                x: pointer.x,
                y: pointer.y
            };

            canvas.selection = false;
            self.isMouseDown = true;
            self.drawnObject = new fabric.Circle({
                left: pointer.x,
                top: pointer.y,
                radius: 1,
                stroke: self.strokeColor,
                fill: 'transparent',
                selectable: true,
                originX: 'center', originY: 'center'
            });

            canvas.add(self.drawnObject);
           
        }
        function mousemove(o) {
            if(!self.isMouseDown) return;
            var pointer = canvas.getPointer(o.e);

            self.drawnObject.set({radius: Math.abs(self.origin.x - pointer.x)});

            
            canvas.renderAll();

        }
        function mouseup(o) {
            self.isMouseDown = false;
            self.drawnObject.setCoords();
            canvas.renderAll();

            canvas.discardActiveObject();
            canvas.setActiveObject(self.drawnObject);

            
            utils.disableAction(self);
        }

        this.enable = function () {
            
            canvas.on('mouse:down', mousedown);
            canvas.on('mouse:move', mousemove);
            canvas.on('mouse:up', mouseup);
            
        }

        this.disable = function () {
            
            self.isMouseDown = false;
            canvas.selection = true;
            canvas.off('mouse:down', mousedown);
            canvas.off('mouse:move', mousemove);
            canvas.off('mouse:up', mouseup);
            actions.setActiveAction(null);

            // self.isMouseDown = false;
            // self.drawnObject.setCoords();
            // canvas.renderAll();

            // canvas.discardActiveObject();
            // canvas.setActiveObject(self.drawnObject);

            
            // utils.disableAction(self);
        }
    }

    return {
        getInstance: function() {
            return new Circle();
        }
    }
});