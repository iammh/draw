define(function() {
    'use strict';
    return {
        disableAction: function(shape) {
            shape.disable()
        },
        finishDrawing(shape) {
            
        },
        clearRadialSnap() {
            
            $("#radialSnapTool").removeClass("activeItem");
            actions.lineOptions().isRadialSnap = false;
        },
        escapeLine(instance) {
            if(instance) {
                instance.disable();
                $("#radialSnapTool").removeClass("activeItem");
                $("#lineTool").removeClass("activeItem");
                $("#snapToGrid").removeClass("activeItem");
                actions.resetLineOptions();
            }
        },
        completeBeforeLeave(instance) {
            console.log("Verifying is path enabled", instance);
            if(instance && instance.type === 'path' && instance.isMouseDown) {
            
                var toastHTML = '<span>Please double click the canvas to complete the path</span><button class="btn-flat toast-action">x</button>';
                M.toast({html: toastHTML});
                return false;
            }
            return true;
        },
        itemSelection(elem, actions, instance) {
            
            var lineOpt = actions.lineOptions();

        

            if(($(elem).hasClass('line') || $(elem).hasClass('radial-snap') || $(elem).hasClass('snap-to-grid')) || $(elem).hasClass('radial-grid-snap')) {
               
                if(lineOpt.isLine) {
                    $("#lineTool").addClass("activeItem");
                    
                }else {
                    
                    $("#lineTool").removeClass("activeItem");
                    if(actions.getActiveAction() && !lineOpt.isRadialSnap && !lineOpt.isSnapToGrid) {
                        
                        actions.getActiveAction().disable();
                    }
                }
    
                if(lineOpt.isRadialSnap) {
                    $("#radialSnapTool").addClass("activeItem");
                }else {
                    $("#radialSnapTool").removeClass("activeItem");
                    
                }
    
                if(lineOpt.isSnapToGrid) {
                    $("#snapToGrid").addClass("activeItem");
                    
                }else {
                    $("#snapToGrid").removeClass("activeItem");
                }
                //

                if(lineOpt.isLine && lineOpt.isSnapToGrid && lineOpt.isRadialSnap) {
                    //select all line
                }else if(lineOpt.isLine && lineOpt.isSnapToGrid) {

                }else if(lineOpt.isLine && lineOpt.isRadialSnap) {

                }else if(lineOpt.isLine) {

                }
                if(actions.getActiveAction()) {
                    
                    actions.getActiveAction().enable();
                }
            }else {
                
                actions.resetLineOptions();
                if(actions.getActiveAction()) {
                    
    
                    if(!$(elem).hasClass('activeItem')) {
                        $(elem).addClass("activeItem");
                        // $(elem).siblings().removeClass("activeItem");
                        if((lineOpt.isSnapToGrid || lineOpt.isRadialSnap) && ($(elem).hasClass('line') || $(elem).hasClass('radial-snap') || $(elem).hasClass('snap-to-grid'))) {
    
                        }else {
                            var siblings = $(elem).siblings();
                            for(var i=0;i < siblings.length; i++) {
                                $(siblings[i]).removeClass("activeItem");
                            }
                        }
                        if(actions.getActiveAction()) {
                        
                            actions.getActiveAction().enable();
                        }
                    }else {
                        $(elem).removeClass("activeItem");
                        if(actions.getActiveAction()) {
                            actions.getActiveAction().disable();
                            actions.setActiveAction(null);
                        }
                        // instance = null;
                    }
                }
            }
            
            
            // utils.setItemSelected(elem);
            // $(elem).addClass("activeItem");
        },
        setUnSelectedItem(elem) {
            $(elem).removeClass("activeItem");
        },
        getMidPoint: function(a,b) {
            var midX = (b.x + a.x)/2;
            var midY = (b.y + a.y)/2;
            return new fabric.Point(midX, midY);
        },
        getDistanceBetweenTwoPoints: function(A,B) {
            var distance = Math.sqrt(((A.x - B.x) * (A.x - B.x)) + ((A.y - B.y) * (A.y - B.y)));

            return distance;
        },
        square: function(value) {
            return value*value;
        },
        findAngle: function(p0,p1,p2) {
            var a = Math.pow(p1.x-p0.x,2) + Math.pow(p1.y-p0.y,2),
                b = Math.pow(p1.x-p2.x,2) + Math.pow(p1.y-p2.y,2),
                c = Math.pow(p2.x-p0.x,2) + Math.pow(p2.y-p0.y,2);
                var divisior = (2*a*b);
                
                if(divisior === 0 || isNaN(div)) {
                    divisior = 0.01;
                }
            return Math.acos( (a+b-c) /  divisior);
          },
          getPoint: function() {
              var activeObject = canvas.getActiveObject();
              if(activeObject) {
                  return new fabric.Point(activeObject.left, activeObject.top + activeObject.getBoundingRect().height);
              }
              else {
                return new fabric.Point(200, 200); 
              }
          },
        generateBackground: function() {
            var options = {
                distance: 20,
                width: canvas.width,
                height: canvas.height,
                param: {
                  stroke: '#ebebeb',
                  strokeWidth: 1,
                  selectable: false,
                  name: 'background-grid'
                }
             };
             var gridLen = options.width / options.distance;
            for (var i = 0; i < gridLen; i++) {
                var distance   = i * options.distance;
                var horizontal = new fabric.Line([ distance, 0, distance, options.width], options.param);
                var vertical   = new fabric.Line([ 0, distance, options.width, distance], options.param);
                canvas.add(horizontal);
                canvas.add(vertical);
                if(i%5 === 0){
                  horizontal.set({stroke: '#cccccc'});
                  vertical.set({stroke: '#cccccc'});
                };
                
              }
        },
        getAllObjects: function() {
            return canvas.getObjects().filter((i) => { return i.name !== 'background-grid'});
        }
    }
});