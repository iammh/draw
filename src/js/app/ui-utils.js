define([], function(require, factory) {
    'use strict';
    return {
        imageUploadDOM: function () {
            return '<div id="imageUploadModal" class="modal">'+
                '<div class="modal-content">'+
                '<h4>Image Upload</h4>'+
                '<form action="#">'+
                    '<div class="file-field input-field">'+
                        '<div class="btn">'+
                        '<span>File</span>'+
                        '<input type="file" id="imageFileInput">'+
                        '</div>'+
                        '<div class="file-path-wrapper">'+
                        '<input class="file-path validate" type="text">'+
                        '</div>'+
                    '</div>'+
                    '</form>'+
                '</div>'+
                '<div class="modal-footer">'+
                '<a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>'+
                '</div>'+
            '</div>';
        },
        radialAnglePopUpDOM: function (angle) {
            var fSelected, tSelected , fvSelected , niSelected = '';
            switch(angle) {
                case 15:
                    fSelected = 'selected';
                    tSelected = '';
                    fvSelected = '';
                    niSelected = '';
                    break;
                case 30:
                    fSelected = '';
                    tSelected = 'selected';
                    fvSelected = '';
                    niSelected = '';
                    break;
                case 45:
                    fSelected = '';
                    tSelected = '';
                    fvSelected = 'selected';
                    niSelected = '';
                    break;
                case 90:
                    fSelected = '';
                    tSelected = '';
                    fvSelected = '';
                    niSelected = 'selected';
                    break;
            }
            return '<div id="radialAngleModal" class="modal">'+
            '<div class="modal-content">'+
                    '<label>Select Angle</label>'+
                    '<select class="browser-default" id="angleSelectBox">'+
                        // '<option value="15" disabled selected>Choose your option</option>'+
                        '<option value="15" '+fSelected+'>15</option>'+
                        '<option value="30"'+tSelected+'>30</option>'+
                        '<option value="45" '+fvSelected+'>45</option>'+
                        '<option value="90" '+niSelected+'>90</option>'+
                    '</select>'+
                '</div>'+
            '<div class="modal-footer">'+
                '<a href="#!" class="modal-close waves-effect grey darker waves-green btn-flat" style="margin-right: 6px;">Close</a>'+
                '<a href="#!" class="modal-close waves-effect blue primary waves-green btn-flat" onclick="actions.clearRadialSnap()">Clear</a>'+
            '</div>'+
        '</div>';
        }
    }
});