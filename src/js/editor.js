define([
    'jquery',
    'fabric',
    'actions'
], function($, fabric, actions) {
    'use strict';
    fabric.objectCaching = false;
    window.canvas = new fabric.Canvas('c', {
        background: 'transparent',
        width: window.innerWidth - 38,
        height: window.innerHeight,
        targetFindTolerance: 4,
        hoverCursor: 'pointer'
    });
    
    window.fabric = fabric;
    // canvas.on({
    //     'object:moving': function(e) {
    //       e.target.opacity = 1;
    //       e.target.stroke = 'red';
    //     },
    //     'object:modified': function(e) {
    //       e.target.opacity = 1;
    //       e.target.stroke = '#ff0000';
    //       e.target.strokeWidth = 1;
    //     },
    //     'object:scaling': function(e) {
    //         var o = e.target;
    //         if (!o.strokeWidthUnscaled && o.strokeWidth) {
    //             o.strokeWidthUnscaled = o.strokeWidth;
    //         }
    //         if (o.strokeWidthUnscaled) {
    //             o.strokeWidth = o.strokeWidthUnscaled / o.scaleX;
    //         }
    //     }
    //   });
    canvas.on('object:scaling', (e) => {
        var o = e.target;
        if (!o.strokeWidthUnscaled && o.strokeWidth) {
          o.strokeWidthUnscaled = o.strokeWidth;
        }
        if (o.strokeWidthUnscaled) {
          o.strokeWidth = o.strokeWidthUnscaled / o.scaleX;
        }
      });
      
    
    actions.init();
    window.actions = actions;
});