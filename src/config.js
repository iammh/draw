requirejs.config({
    baseUrl: 'src/js',
    paths: {
        'jquery': ['lib/jquery.min'],
        'toolbar': ['app/toolbar'],
        'dialog': ['app/dialog'],
        'editor': ['editor'],
        'fabric': ['lib/fabric.min'],
        'utils': ['app/utils'],
        'actions': ['app/actions'],
        'line': ['app/shapes/line'],
        'rectangle': ['app/shapes/rectangle'],
        'circle': ['app/shapes/circle'],
        'snapToGrid': ['app/shapes/snap-to-grid-line'],
        'radialSnap': ['app/shapes/radial-snap-line'],
        'path': ['app/shapes/path'],
        'radialGridSnap': ['app/shapes/rs-line'],
        'ellipse': ['app/shapes/ellipse'],
        'arc': ['app/shapes/arc'],
        'colors': ['app/colors/shapes'],
        'mat': ['lib/materialize.min'],
        'uiUtils': ['app/ui-utils']
    
    }
});

require(['editor', 'toolbar','mat','jquery'], function(editor, toolbar, mat,$) {
    toolbar();
    $("#canvas-container").bind('contextmenu',function(e) {
        return false;
    });
    setTimeout(function() {
        $('.tooltipped').tooltip({
            enterDelay: 100
        });
    }, 1000);

    

    
    
});